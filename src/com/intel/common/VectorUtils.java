package com.intel.common;

import java.security.SecureRandom;

public class VectorUtils {
	public static String printVector(int[] v) {
		System.out.print("[ ");
		for (int e : v) {
			System.out.print(e + " ");
		}
		System.out.print("]");
		System.out.println();
		return "";
	}

	public static <T> String printVector(T[] v) {
		System.out.print("[ ");
		for (T e : v) {
			System.out.print(e + " ");
		}
		System.out.print("]");
		System.out.println();
		return "";
	}

	public static <T extends Comparable<T>> boolean isVectorSorted(T[] v) {
		boolean sorted = true;
		for (int i = 0; i < v.length - 1; i++) {
			if (v[i].compareTo(v[i + 1]) > 0) {
				sorted = false;
				break;
			}
		}
		return sorted;
	}

	public static Integer[] genRandomVectorOfInt(int size, int elemUpperBound) {
		Integer[] v = new Integer[size];
		SecureRandom r = new SecureRandom();
		for (int i = 0; i < v.length; i++) {
			v[i] = r.nextInt(elemUpperBound);
		}
		return v;
	}

	public static Integer[] genSortedVectorOfInt(int size) {
		Integer[] v = new Integer[size];
		for (int i = 0; i < v.length; i++) {
			v[i] = i;
		}
		return v;
	}

	public static Integer[] genReverseVectorOfInt(int size) {
		Integer[] v = new Integer[size];
		for (int i = 0; i < v.length; i++) {
			v[i] = v.length - i;
		}
		return v;
	}

	public static <T extends Comparable<T>> void swapElements(T[] array, int index1, int index2) {
		T tmp = array[index1];
		array[index1] = array[index2];
		array[index2] = tmp;
	}
}
