package com.intel.common;

import java.io.Closeable;
import java.io.IOException;

public class CommonUtils {
	public static void closeStreams(Closeable... resources) {
		for (Closeable s : resources) {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					System.out.println("error while closing: " + s.toString());
					e.printStackTrace();
				}
			}
		}
	}
}
