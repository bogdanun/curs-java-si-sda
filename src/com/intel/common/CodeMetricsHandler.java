package com.intel.common;

public class CodeMetricsHandler {
	int nrOfOps = 0;

	public int getNrOps() {
		return nrOfOps;
	}

	public void incOps(int nr) {
		nrOfOps += nr;
	}

	public void reset() {
		nrOfOps = 0;
	}
}
