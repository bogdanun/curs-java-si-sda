package com.intel.curssda.sorting;

import com.intel.common.VectorUtils;

public class BubbleSorter extends MetricsSorter {

	@Override
	public <T extends Comparable<T>> void sort(T[] array) {
		boolean sorted = false;
		while (!sorted) {
			sorted = true;
			incOps(2);
			for (int i = 0; i < array.length - 1; i++) {
				if (array[i].compareTo(array[i + 1]) > 0) {
					VectorUtils.swapElements(array, i, i + 1);
					sorted = false;
					incOps(6);
				} else {
					incOps(2);
				}
			}
		}
	}
}
