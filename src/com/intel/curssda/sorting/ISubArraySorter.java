package com.intel.curssda.sorting;

public interface ISubArraySorter extends ISorter {
	public <T extends Comparable<T>> void sortSubArray(T[] array, int begin, int length);
}
