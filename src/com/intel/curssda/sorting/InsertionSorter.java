package com.intel.curssda.sorting;

import com.intel.common.VectorUtils;

public class InsertionSorter extends MetricsSorter {

	@Override
	public <T extends Comparable<T>> void sort(T[] array) {
		sortSubArray(array, 0, array.length);
	}

	public <T extends Comparable<T>> void sortSubArray(T[] array, int begin, int length) {
		for (int i = begin + 1; i <= begin + length - 1; i++) {
			int j = i;
			incOps(2);
			while (j > begin && array[j - 1].compareTo(array[j]) > 0) {
				VectorUtils.swapElements(array, j, j - 1);
				j -= 1;
				incOps(5);
			}
		}
	}
}
