package com.intel.curssda.sorting.mergesort;

import com.google.inject.Inject;
import com.intel.curssda.sorting.ISubArraySorter;
import com.intel.curssda.sorting.mergesort.strategy.IStrategy;

public class MergeSorter implements ISubArraySorter {

	IStrategy strategy;

	@Inject
	public MergeSorter(IStrategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public <T extends Comparable<T>> void sort(T[] array) {
		T[] aux = array.clone();
		if (array.length == 0 || array.length == 1) {
			// empty vector and 1 element vectors are already sorted
			return;
		}
		mergeSort(array, 0, array.length - 1, aux);
		System.arraycopy(aux, 0, array, 0, aux.length);
	}

	private <T extends Comparable<T>> void mergeSort(T[] array, int begin, int end, T[] aux) {
		if (strategy.doneDividing(array, begin, end)) {
			strategy.sortBaseDivision(array, begin, end, aux);
			return;
		}
		int middle = strategy.divideIndex(begin, end);
		mergeSort(array, begin, middle, aux);
		mergeSort(array, middle + 1, end, aux);
		strategy.merge(array, begin, middle, end, aux);
	}

	@Override
	public <T extends Comparable<T>> void sortSubArray(T[] array, int begin, int length) {
		// TODO Auto-generated method stub

	}

}
