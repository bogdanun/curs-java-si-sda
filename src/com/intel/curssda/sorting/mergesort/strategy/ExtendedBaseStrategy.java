package com.intel.curssda.sorting.mergesort.strategy;

import com.google.inject.Inject;
import com.intel.curssda.sorting.ISubArraySorter;

public class ExtendedBaseStrategy extends BasicStrategy {
	protected int baseCaseLength;
	protected ISubArraySorter baseCaseSorter;

	@Inject
	public ExtendedBaseStrategy(Integer baseCaseLength, ISubArraySorter baseCaseSorter) {
		this.baseCaseLength = baseCaseLength;
		this.baseCaseSorter = baseCaseSorter;
	}

	@Override
	public <T extends Comparable<T>> boolean doneDividing(T[] array, int begin, int end) {
		assert end >= begin;
		return end - begin + 1 <= baseCaseLength;
	}

	@Override
	public <T extends Comparable<T>> void sortBaseDivision(T[] array, int begin, int end, T[] aux) {
		assert end - begin + 1 <= baseCaseLength;
		baseCaseSorter.sortSubArray(array, begin, end - begin + 1);
		System.arraycopy(array, begin, aux, begin, end - begin + 1);
	}
}
