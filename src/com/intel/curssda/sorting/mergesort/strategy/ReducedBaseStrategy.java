package com.intel.curssda.sorting.mergesort.strategy;

public class ReducedBaseStrategy extends BasicStrategy {

	@Override
	public <T extends Comparable<T>> boolean doneDividing(T[] array, int begin, int end) {
		assert end >= begin;
		return end - begin + 1 < 2;
	}

	@Override
	public <T extends Comparable<T>> void sortBaseDivision(T[] array, int begin, int end, T[] aux) {
		assert end - begin + 1 < 2;
	}
}
