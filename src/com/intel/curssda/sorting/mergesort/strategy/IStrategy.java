package com.intel.curssda.sorting.mergesort.strategy;

public interface IStrategy {

	<T extends Comparable<T>> boolean doneDividing(T[] array, int begin, int end);

	<T extends Comparable<T>> void sortBaseDivision(T[] array, int begin, int end, T[] aux);

	int divideIndex(int begin, int end);

	<T extends Comparable<T>> void merge(T[] array, int begin, int middle, int end, T[] aux);

}
