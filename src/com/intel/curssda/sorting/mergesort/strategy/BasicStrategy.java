package com.intel.curssda.sorting.mergesort.strategy;

import com.intel.common.VectorUtils;

public class BasicStrategy implements IStrategy {

	@Override
	public <T extends Comparable<T>> boolean doneDividing(T[] array, int begin, int end) {
		assert end >= begin;
		return end - begin + 1 < 3;
	}

	@Override
	public <T extends Comparable<T>> void sortBaseDivision(T[] array, int begin, int end, T[] aux) {
		assert end - begin + 1 < 3;
		if (end - begin + 1 == 2) {
			if (array[begin].compareTo(array[end]) > 0) {
				VectorUtils.swapElements(array, begin, end);
				VectorUtils.swapElements(aux, begin, end);
			}
		}
	}

	@Override
	public int divideIndex(int begin, int end) {
		assert end > begin;
		return begin + (end - begin) / 2;
	}

	@Override
	public <T extends Comparable<T>> void merge(T[] array, int begin, int middle, int end, T[] aux) {
		assert begin <= middle && middle <= end;
		int iter1 = begin;
		int iter2 = middle + 1;
		int iteraux = begin;
		while (iteraux <= end) {
			if (iter2 <= end && array[iter1].compareTo(array[iter2]) > 0) {
				aux[iteraux] = array[iter2];
				iter2++;
			} else if (iter1 <= middle) {
				aux[iteraux] = array[iter1];
				iter1++;
			}
			iteraux++;
		}
		System.arraycopy(aux, begin, array, begin, end - begin + 1);
	}
}
