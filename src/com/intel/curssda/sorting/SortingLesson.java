package com.intel.curssda.sorting;

import java.io.PrintWriter;
import java.util.HashMap;

import com.intel.common.CommonUtils;
import com.intel.common.VectorUtils;
import com.intel.cursjava.Lesson;

public class SortingLesson implements Lesson {

	private HashMap<String, Integer[]> createIntegerTestData(int size, int elemUpperBound) {
		HashMap<String, Integer[]> testData = new HashMap<String, Integer[]>();
		testData.put("Random vector", VectorUtils.genRandomVectorOfInt(size, elemUpperBound));
		testData.put("Sorted vector", VectorUtils.genSortedVectorOfInt(size));
		testData.put("Reverse vector", VectorUtils.genReverseVectorOfInt(size));
		return testData;
	}

	private String measureSortersOnIntVectors(HashMap<String, Integer[]> testData, MetricsSorter... sorters) {
		StringBuilder result = new StringBuilder();
		for (MetricsSorter s : sorters) {
			for (String dataType : testData.keySet()) {
				resetSortersMetrics(sorters);
				System.out.println("data type: " + dataType);
				Integer[] vCopy = testData.get(dataType).clone();
				s.sort(vCopy);
				// VectorUtils.printVector(vCopy);
				System.out.println(s.getClass().getName() + " nr of operations: " + s.getNrOfOps());
				result.append(s.getNrOfOps() + ", ");
			}
		}
		result.append("\n");
		return result.toString();

	}

	private void resetSortersMetrics(MetricsSorter... sorters) {
		for (MetricsSorter s : sorters) {
			s.resetMetrics();
		}
	}

	@Override
	public void runAssignment() {
		PrintWriter writer = null;
		String metrics = null;
		try {
			writer = new PrintWriter("sorting_metrics.txt", "UTF-8");
			BubbleSorter bs = new BubbleSorter();
			InsertionSorter is = new InsertionSorter();
			for (int i = 0; i < 101; i += 100) {
				System.out.println("vecttor size: " + i);
				writer.write(i + ", ");
				HashMap<String, Integer[]> testData = createIntegerTestData(i, i + 10);
				metrics = measureSortersOnIntVectors(testData, bs, is);
				writer.write(metrics);
			}
		} catch (Exception e) {
			;
		} finally {
			CommonUtils.closeStreams(writer);
		}
	}
}
