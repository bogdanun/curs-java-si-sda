package com.intel.curssda.sorting;

import com.intel.common.CodeMetricsHandler;

public abstract class MetricsSorter implements ISubArraySorter {

	CodeMetricsHandler cms = new CodeMetricsHandler();

	@Override
	public <T extends Comparable<T>> void sort(T[] array) {
	}

	public void addMetricsHandler(CodeMetricsHandler cms) {
		this.cms = cms;
	}

	public void incOps(int nr) {
		cms.incOps(nr);
	}

	public int getNrOfOps() {
		return cms.getNrOps();
	}

	public void resetMetrics() {
		cms.reset();
	}

	@Override
	public <T extends Comparable<T>> void sortSubArray(T[] array, int begin, int length) {
	}

}
