package com.intel.curssda.sorting;

public interface ISorter {

	public <T extends Comparable<T>> void sort(T[] array);

}
