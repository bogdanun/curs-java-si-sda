package com.intel.curssda.sorting;

import com.intel.common.VectorUtils;

public class HeapSorter implements ISorter {
	public static final int HEAP_SIZE_EXCEEDED = -1;

	@Override
	public <T extends Comparable<T>> void sort(T[] vect) {
		for (int i = 1; i <= vect.length - 1; i++) {
			makeHeap(vect, i);
		}
		VectorUtils.printVector(vect);
		for (int i = vect.length - 1; i > 1; i--) {
			// extract max element from heap
			VectorUtils.swapElements(vect, 1, i);
			bubbleDownHeapRoot(vect, i - 1);
		}
	}

	private <T extends Comparable<T>> int getMaxChildIndex(T[] heap, int heapSize, int currentIndex) {
		int leftChildIndex = 2 * currentIndex;
		int rightChildIndex = 2 * currentIndex + 1;
		if (leftChildIndex > heapSize) {
			return HEAP_SIZE_EXCEEDED;
		}
		if (leftChildIndex <= heapSize && rightChildIndex > heapSize) {
			return leftChildIndex;
		}
		T leftChild = heap[leftChildIndex];
		T rightChild = heap[rightChildIndex];
		if (leftChild.compareTo(rightChild) > 0) {
			return leftChildIndex;
		} else {
			return rightChildIndex;
		}
	}

	private <T extends Comparable<T>> void bubbleDownHeapRoot(T[] vect, int heapSize) {
		int currentNodeIndex = 1;
		do {
			int swapIndex = getMaxChildIndex(vect, heapSize, currentNodeIndex);
			if (swapIndex == HEAP_SIZE_EXCEEDED) {
				break;
			}
			if (vect[currentNodeIndex].compareTo(vect[swapIndex]) < 0) {
				VectorUtils.swapElements(vect, swapIndex, currentNodeIndex);
				currentNodeIndex = swapIndex;
			} else {
				break;
			}
		} while (true);
	}

	private <T extends Comparable<T>> void makeHeap(T[] vect, int i) {
		int currentIndex = i;
		while (hasHeapParent(currentIndex) && vect[currentIndex].compareTo(getHeapParent(vect, currentIndex)) > 0) {
			swapHeapElementWithParent(vect, currentIndex);
			currentIndex /= 2;
		}
	}

	private <T extends Comparable<T>> void swapHeapElementWithParent(T[] heap, int i) {
		assert i > 1;
		if (i > 1) {
			T tmp = heap[i];
			heap[i] = heap[i / 2];
			heap[i / 2] = tmp;
		}
	}

	private <T extends Comparable<T>> T getHeapParent(T[] heap, int i) {
		assert i > 1;
		if (i > 1) {
			return heap[i / 2];
		}
		return null;
	}

	private boolean hasHeapParent(int i) {
		return i > 1;
	}
}
