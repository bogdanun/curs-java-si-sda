package com.intel.curssda.listelements;

public class ListElement<T extends Comparable<T>> implements IListElement<T> {
	protected T info = null;
	protected IListElement<T> next = null;

	public ListElement(T info) {
		setInfo(info);
	}

	@Override
	public T getInfo() {
		return info;
	}

	@Override
	public IListElement<T> getNext() {
		return next;
	}

	@Override
	public void setNext(IListElement<T> next) {
		this.next = next;

	}

	@Override
	public void setInfo(T info) {
		this.info = info;

	}
}
