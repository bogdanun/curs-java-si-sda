package com.intel.curssda.listelements;

public interface IListElementWithInsertionOrder<T extends Comparable<T>> extends IListElement<T> {

	public IListElementWithInsertionOrder<T> getNextInserted();

	public void setNextInserted(IListElementWithInsertionOrder<T> nextInserted);

}
