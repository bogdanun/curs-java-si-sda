package com.intel.curssda.listelements;

public class ListElementWithInsertionOrderCreator implements IListElementCreator {

	@Override
	public <T extends Comparable<T>> IListElementWithInsertionOrder<T> createListElement(T info) {
		return new ListElemenWithtInsertionOrder<T>(info);
	}
}
