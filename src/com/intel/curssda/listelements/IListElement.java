package com.intel.curssda.listelements;

public interface IListElement<T extends Comparable<T>> {
	public T getInfo();

	public IListElement<T> getNext();

	public void setNext(IListElement<T> next);

	public void setInfo(T info);
}
