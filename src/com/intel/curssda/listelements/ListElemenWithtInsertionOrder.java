package com.intel.curssda.listelements;

public class ListElemenWithtInsertionOrder<T extends Comparable<T>> extends ListElement<T>
		implements IListElementWithInsertionOrder<T> {

	protected IListElementWithInsertionOrder<T> nextInserted = null;

	public ListElemenWithtInsertionOrder(T info) {
		super(info);
	}

	@Override
	public IListElementWithInsertionOrder<T> getNextInserted() {
		return nextInserted;
	}

	@Override
	public void setNextInserted(IListElementWithInsertionOrder<T> nextInserted) {
		this.nextInserted = nextInserted;
	}

}
