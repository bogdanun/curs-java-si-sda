package com.intel.curssda.listelements;

public interface IListElementCreator {
	public <T extends Comparable<T>> IListElement<T> createListElement(T info);

}
