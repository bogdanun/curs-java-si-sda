package com.intel.curssda.listelements;

public class ListElementCreator implements IListElementCreator {

	@Override
	public <T extends Comparable<T>> IListElement<T> createListElement(T info) {
		return new ListElement<T>(info);
	}
}
