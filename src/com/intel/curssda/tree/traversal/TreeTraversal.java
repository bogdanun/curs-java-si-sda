package com.intel.curssda.tree.traversal;

import java.util.LinkedList;
import java.util.List;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public abstract class TreeTraversal<T> {

	protected List<T> traversalOrder = new LinkedList<T>();

	public void traverse(IBinaryTreeNode<T> node) {
		if (node == null) {
			return;
		}
		preOrderHook(node);
		IBinaryTreeNode<T>[] children = node.getSubtrees();
		for (int i = 0; i < children.length; i++) {
			traverse(children[i]);
			if (i < children.length - 1) {
				inOrderHook(node);
			}

		}
		postOrderHook(node);
	}

	public List<T> getTraversalOrder() {
		return traversalOrder;
	}

	public void inOrderHook(IBinaryTreeNode<T> node) {
	}

	public void preOrderHook(IBinaryTreeNode<T> node) {
	}

	public void postOrderHook(IBinaryTreeNode<T> node) {
	}
}