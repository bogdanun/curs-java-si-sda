package com.intel.curssda.tree.traversal;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public class InOrderBinaryTreeTraversal<T> extends TreeTraversal<T> {

	@Override
	public void inOrderHook(IBinaryTreeNode<T> node) {
		System.out.print(node.getInfo() + " ");
		traversalOrder.add(node.getInfo());
	}
}
