package com.intel.curssda.tree.traversal;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public class PreOrderBinaryTreeTraversal<T> extends TreeTraversal<T> {

	@Override
	public void preOrderHook(IBinaryTreeNode<T> node) {
		System.out.print(node.getInfo() + " ");
		traversalOrder.add(node.getInfo());
	}
}
