package com.intel.curssda.tree.traversal;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public class PostOrderBinaryTreeTraversal<T> extends TreeTraversal<T> {

	@Override
	public void postOrderHook(IBinaryTreeNode<T> node) {
		System.out.print(node.getInfo() + " ");
		traversalOrder.add(node.getInfo());
	}
}
