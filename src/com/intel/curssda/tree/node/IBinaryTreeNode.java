package com.intel.curssda.tree.node;

public interface IBinaryTreeNode<T> extends ITreeNode<T> {
	public void setInfo(T info);

	public void setLeft(IBinaryTreeNode<T> left);

	public void setright(IBinaryTreeNode<T> right);

	public IBinaryTreeNode<T> getLeft();

	public IBinaryTreeNode<T> getRight();

	public T getInfo();

	public IBinaryTreeNode<T>[] getSubtrees();

}
