package com.intel.curssda.tree.node;

public class BinaryTreeNode<T> implements IBinaryTreeNode<T> {

	protected T info;
	protected IBinaryTreeNode<T> left, right;

	public BinaryTreeNode(T info, IBinaryTreeNode<T> left, IBinaryTreeNode<T> right) {
		setInfo(info);
		setLeft(left);
		setright(right);
	}

	@Override
	public void setInfo(T info) {
		this.info = info;

	}

	@Override
	public void setLeft(IBinaryTreeNode<T> left) {
		this.left = left;

	}

	@Override
	public void setright(IBinaryTreeNode<T> right) {
		this.right = right;

	}

	@Override
	public void printInfo() {
		System.out.print(info);

	}

	@Override
	public T getInfo() {
		return info;
	}

	@Override
	public IBinaryTreeNode<T> getLeft() {
		return left;
	}

	@Override
	public IBinaryTreeNode<T> getRight() {
		return right;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IBinaryTreeNode<T>[] getSubtrees() {
		return new IBinaryTreeNode[] { left, right };
	}

}
