package com.intel.curssda.tree;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public class BinaryTree<T> implements IBinaryTree<T> {

	protected IBinaryTreeNode<T> root;

	@Override
	public void setRoot(IBinaryTreeNode<T> root) {
		this.root = root;
	}

	@Override
	public IBinaryTreeNode<T> getRoot() {
		return root;
	}

}
