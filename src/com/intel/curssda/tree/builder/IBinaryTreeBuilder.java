package com.intel.curssda.tree.builder;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public interface IBinaryTreeBuilder<T> extends ITreeBuilder<T> {
	@Override
	public IBinaryTreeNode<T> buildAndGetRoot();
}
