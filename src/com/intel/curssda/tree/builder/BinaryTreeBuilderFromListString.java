package com.intel.curssda.tree.builder;

import com.intel.curssda.tree.node.BinaryTreeNode;
import com.intel.curssda.tree.node.IBinaryTreeNode;

public class BinaryTreeBuilderFromListString<T> implements IBinaryTreeBuilder<T> {
	public static final char LEFT_PARAN = '(';
	public static final char RIGHT_PARAN = ')';
	public static final int INDEX_NOT_FOUND = -1;

	protected String stringRepr;

	public BinaryTreeBuilderFromListString(String repr) {
		stringRepr = repr;
	}

	protected int getLeftSubtreeEndIndex(int leftSubtreeStartIndex, String repr) {
		int leftParanthesisOpened = 0;
		for (int i = leftSubtreeStartIndex; i < repr.length() - 1; i++) {
			char c = repr.charAt(i);
			if (c == LEFT_PARAN) {
				leftParanthesisOpened++;
			} else if (c == RIGHT_PARAN) {
				leftParanthesisOpened--;
			}
			if (leftParanthesisOpened == 0) {
				return i;
			}
		}
		return INDEX_NOT_FOUND;
	}

	protected boolean isReprStringLeaf(String repr) {
		return repr.indexOf(LEFT_PARAN, 1) < 0;
	}

	protected String getInfoFromLeaf(String repr) {
		assert isReprStringLeaf(repr);
		// if the leaf is like ()
		if (repr.length() == 2) {
			return null;
		} else { // if the leaf is like (info)
			return repr.substring(1, repr.length() - 1);
		}
	}

	protected IBinaryTreeNode<String> transformStringToNodes(String repr) {
		if (isReprStringLeaf(repr)) {
			String info = getInfoFromLeaf(repr);
			if (info == null) {
				return null;
			} else {
				return new BinaryTreeNode<String>(info, null, null);
			}
		}

		// ignore leftmost and rightmost parentheses
		int leftSubtreeStartIndex = repr.indexOf(LEFT_PARAN, 1);
		String info = repr.substring(1, leftSubtreeStartIndex);

		int leftSubtreeEndIndex = getLeftSubtreeEndIndex(leftSubtreeStartIndex, repr);
		IBinaryTreeNode<String> leftSubtree = transformStringToNodes(
				repr.substring(leftSubtreeStartIndex, leftSubtreeEndIndex + 1));
		IBinaryTreeNode<String> rightSubtree = transformStringToNodes(
				repr.substring(leftSubtreeEndIndex + 1, repr.length() - 1));

		return new BinaryTreeNode<String>(info, leftSubtree, rightSubtree);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IBinaryTreeNode<T> buildAndGetRoot() {
		return (IBinaryTreeNode<T>) transformStringToNodes(this.stringRepr);
	}
}
