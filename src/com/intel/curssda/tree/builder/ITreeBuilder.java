package com.intel.curssda.tree.builder;

import com.intel.curssda.tree.node.ITreeNode;

public interface ITreeBuilder<T> {
	public ITreeNode<T> buildAndGetRoot();
}
