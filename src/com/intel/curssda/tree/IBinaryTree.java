package com.intel.curssda.tree;

import com.intel.curssda.tree.node.IBinaryTreeNode;

public interface IBinaryTree<T> extends ITree<T> {
	public void setRoot(IBinaryTreeNode<T> root);
	
	public IBinaryTreeNode<T> getRoot();

}
