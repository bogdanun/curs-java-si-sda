package com.intel.curssda.hashset;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.intel.curssda.hash.IHashAlgoritm;
import com.intel.curssda.listelements.IListElementCreator;
import com.intel.curssda.listelements.IListElementWithInsertionOrder;

public class HashSetWithInsertionOrder<T extends Comparable<T>> extends HashSet<T> {

	IListElementWithInsertionOrder<T> firstInsertedMarker;
	IListElementWithInsertionOrder<T> lastInsertedElement;

	@SuppressWarnings("unchecked")
	@Inject
	public HashSetWithInsertionOrder(@Named("HashSet Dimension") int size, IHashAlgoritm hashAlgorithm,
			IListElementCreator listElementCreator) {
		super(size, hashAlgorithm, listElementCreator);
		firstInsertedMarker = (IListElementWithInsertionOrder<T>) this.listElementCreator.createListElement(null);
		lastInsertedElement = firstInsertedMarker;
	}

	@Override
	protected void addHook(T info) {
		lastInsertedElement
				.setNextInserted((IListElementWithInsertionOrder<T>) this.listElementCreator.createListElement(info));
		lastInsertedElement = lastInsertedElement.getNextInserted();
	}

	@Override
	protected void removeHook(T info) {
		IListElementWithInsertionOrder<T> iterElement = firstInsertedMarker;
		while (iterElement.getNextInserted() != null && iterElement.getNextInserted().getInfo().compareTo(info) != 0) {
			iterElement = iterElement.getNextInserted();
		}
		// delete the element containing info
		iterElement.setNextInserted(iterElement.getNextInserted().getNextInserted());
	}

	public IListElementWithInsertionOrder<T> getFirstInserted() {
		return firstInsertedMarker.getNextInserted();
	}

	public void printInsertionOrder() {
		System.out.println("Insertion order display:");
		if (firstInsertedMarker.getNextInserted() == null) {
			System.out.println("hashset is empty");
			return;
		}
		IListElementWithInsertionOrder<T> iterElement = firstInsertedMarker.getNextInserted();
		System.out.print("hashset insertion order : ");
		while (iterElement != null) {
			System.out.print(iterElement.getInfo() + " ");
			iterElement = iterElement.getNextInserted();
		}
		System.out.println();
	}
}
