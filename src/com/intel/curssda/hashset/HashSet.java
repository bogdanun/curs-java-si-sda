package com.intel.curssda.hashset;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.intel.curssda.hash.IHashAlgoritm;
import com.intel.curssda.listelements.IListElement;
import com.intel.curssda.listelements.IListElementCreator;

public class HashSet<T extends Comparable<T>> implements IHashSet<T> {

	protected int size = 0;
	protected int nrOfElements = 0;
	protected IListElement<T>[] set;
	protected IHashAlgoritm hashAlgorithm;
	protected IListElementCreator listElementCreator;

	@Inject
	public HashSet(@Named("HashSet Dimension") int size, IHashAlgoritm hashAlgorithm,
			IListElementCreator listElementCreator) {
		assert size > 0;
		this.size = size;
		this.hashAlgorithm = hashAlgorithm;
		this.listElementCreator = listElementCreator;
		allocInitialHashSet();
	}

	@SuppressWarnings("unchecked")
	private void allocInitialHashSet() {
		// Unchecked type
		set = new IListElement[this.size];
		for (int i = 0; i < set.length; i++) {
			set[i] = this.listElementCreator.createListElement(null);
		}
	}

	private IListElement<T> getHashBucket(T info) {
		int hash = hashAlgorithm.getHashCode(info);
		return set[hash];
	}

	@Override
	public boolean isEmpty() {
		assert nrOfElements >= 0;
		return nrOfElements == 0;
	}

	@Override
	public boolean contains(T info) {
		IListElement<T> bucket = getHashBucket(info);
		while (bucket != null) {
			if (bucket.getInfo() != null && bucket.getInfo().compareTo(info) == 0) {
				return true;
			}
			bucket = (IListElement<T>) bucket.getNext();
		}
		return false;
	}

	protected void addHook(T info) {
		// Override in subclasses
	}

	@Override
	public void add(T info) {
		assert nrOfElements >= 0;
		// if the element is already in the set, do nothing
		if (contains(info)) {
			return;
		}

		addHook(info);

		// if the element is not in the set
		IListElement<T> bucket = getHashBucket(info);
		// if there is no element in the bucket
		if (bucket.getInfo() == null) {
			bucket.setInfo(info);
		} else {
			// if there are already some elements in the bucket
			while (bucket.getNext() != null) {
				bucket = (IListElement<T>) bucket.getNext();
			}
			// add new element to bucket
			IListElement<T> newElem = this.listElementCreator.createListElement(info);
			newElem.setInfo(info);
			bucket.setNext(newElem);
		}
		nrOfElements++;
	}

	@Override
	public void remove(T info) {
		assert nrOfElements > 0;
		// if the element is not in the set, do nothing
		if (!contains(info)) {
			return;
		}

		removeHook(info);

		IListElement<T> bucket = getHashBucket(info);

		// the element is the top of the bucket
		if (bucket.getInfo().compareTo(info) == 0) {
			// if the bucket has more than 1 element
			if (bucket.getNext() != null) {
				int hash = hashAlgorithm.getHashCode(info);
				set[hash] = (IListElement<T>) bucket.getNext();
			} else { // if the bucket has just one element
				bucket.setInfo(null);
			}
		} else {
			// the element is not the top of the bucket
			while (bucket.getNext().getInfo().compareTo(info) != 0) {
				bucket = (IListElement<T>) bucket.getNext();
			}
			bucket.setNext(bucket.getNext().getNext());
		}
		nrOfElements--;
	}

	protected void removeHook(T info) {
		// Override in subclasses

	}

	public void printInternalStructure() {
		System.out.println("Bucket display:");
		if (nrOfElements == 0) {
			System.out.println("hashset is empty");
			return;
		}
		System.out.println("hashset non null buckets: ");
		for (int i = 0; i < set.length; i++) {
			IListElement<T> bucket = set[i];
			if (bucket.getInfo() != null) {
				System.out.print("bucket " + i + " : ");
				printBucketContents(bucket);
			}
		}
	}

	private void printBucketContents(IListElement<T> bucket) {
		while (bucket != null) {
			System.out.print(bucket.getInfo() + " ");
			bucket = (IListElement<T>) bucket.getNext();
		}
		System.out.println();
	}
}
