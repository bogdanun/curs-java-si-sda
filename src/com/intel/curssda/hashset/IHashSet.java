package com.intel.curssda.hashset;

public interface IHashSet<T extends Comparable<T>> {
	public boolean isEmpty();

	public boolean contains(T elem);

	public void add(T elem);

	public void remove(T elem);

}
