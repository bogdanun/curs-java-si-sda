package com.intel.curssda.hash;

public interface IHashAlgoritm {
	public <T> int getHashCode(T element);
}
