package com.intel.curssda.hash;

import com.google.inject.Inject;
import com.google.inject.name.Named;;

public class IntegerModuloHashAlgorithm implements IHashAlgoritm {

	private int divider;

	@Inject
	public IntegerModuloHashAlgorithm(@Named("Hash Prime Divider") int divider) {
		this.divider = divider;
	}

	@Override
	public <T> int getHashCode(T element) {
		if (element instanceof Integer) {
			return (Integer) element % divider;
		} else {
			throw new ClassCastException("Expected an Integer element");
		}
	}
}
