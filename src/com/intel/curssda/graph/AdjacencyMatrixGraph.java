package com.intel.curssda.graph;

import java.util.ArrayList;
import com.google.inject.name.Named;
import com.intel.curssda.graph.utils.IVertexMapping;
import com.intel.curssda.graph.utils.IVertexMarkings;

public class AdjacencyMatrixGraph<T> extends AbstractGraph<T> implements IGraph<T> {

	protected int[][] adjecencyMatrix;

	public AdjacencyMatrixGraph(@Named("Number of vertices") int nrOfVertices,
			@Named("Vertex Map") IVertexMapping<T> vertexMapping,
			@Named("Vertex Markings") IVertexMarkings vertexMarkings) {
		this.nrOfVertices = nrOfVertices;
		adjecencyMatrix = new int[nrOfVertices][nrOfVertices];
		this.vertexMapping = vertexMapping;
		this.vertexMarkings = vertexMarkings;
	}

	public void addDirectedEdge(T vertex1, T vertex2, int... cost) {
		int index1 = vertexMapping.getVertexIndex(vertex1);
		int index2 = vertexMapping.getVertexIndex(vertex2);
		if (cost.length < 1) {
			this.adjecencyMatrix[index1][index2] = 1;
		} else {
			this.adjecencyMatrix[index1][index2] = cost[0];
		}
	}

	public void addEdge(T vertex1, T vertex2, int... cost) {
		if (cost.length < 1) {
			addDirectedEdge(vertex1, vertex2);
			addDirectedEdge(vertex2, vertex1);
		} else {
			addDirectedEdge(vertex1, vertex2, cost[0]);
			addDirectedEdge(vertex2, vertex1, cost[0]);
		}
	}

	@Override
	public Integer[] getAdjecentVertices(int vertexIndex) {
		ArrayList<Integer> adjVertices = new ArrayList<>();
		for (int j = 0; j < nrOfVertices; j++) {
			if (adjecencyMatrix[vertexIndex][j] >= 1) {
				adjVertices.add(j);
			}
		}
		Integer[] result = new Integer[adjVertices.size()];
		int index = 0;
		for (Integer elem : adjVertices) {
			result[index++] = elem;
		}
		return result;
	}

	@Override
	public void addVertex(T vertex) {
		if (vertexMapping.getVertexIndex(vertex) >= 0) {
			return;
		}
		vertexMapping.mapVertex(vertex);
	}

	@Override
	public int getEdgeCost(int vertexIndex1, int vertexIndex2) {
		return this.adjecencyMatrix[vertexIndex1][vertexIndex2];
	}

}
