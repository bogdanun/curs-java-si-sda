package com.intel.curssda.graph;

import com.intel.curssda.graph.utils.IVertexMarkings.VertexType;

public interface IGraph<T> {
	public static final int VERTEX_NOT_FOUND = -1;

	public int findUnvisitedVertex();

	public void markVertex(int vertexIndex, VertexType type);

	public void addDirectedEdge(T vertex1, T vertex2, int... cost);

	public void addEdge(T vertex1, T vertex2, int... cost);

	public void addVertex(T vertex);

	public T getVertexFromIndex(int index);

	public int getEdgeCost(int vertexIndex1, int vertexIndex2);

	public VertexType getVertexMarking(int vertexIndex);

	public Integer[] getAdjecentVertices(int vertexIndex);

}
