package com.intel.curssda.graph;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.name.Named;
import com.intel.curssda.graph.utils.IVertexMapping;
import com.intel.curssda.graph.utils.IVertexMarkings;
import com.intel.curssda.graphelements.IVertex;
import com.intel.curssda.graphelements.Vertex;

public class AdjacencyListsGraph<T extends Comparable<T>> extends AbstractGraph<T> implements IGraph<T> {

	List<IVertex<T>> adjacencyList;

	public AdjacencyListsGraph(@Named("Vertex Map") IVertexMapping<T> vertexMapping,
			@Named("Vertex Markings") IVertexMarkings vertexMarkings) {
		adjacencyList = new ArrayList<>();
		this.vertexMarkings = vertexMarkings;
		this.vertexMapping = vertexMapping;
	}

	@Override
	public void addDirectedEdge(T info1, T info2, int... cost) {
		int index1 = vertexMapping.getVertexIndex(info1);
		int index2 = vertexMapping.getVertexIndex(info2);
		IVertex<T> vertex1 = adjacencyList.get(index1);
		IVertex<T> vertex2 = adjacencyList.get(index2);
		vertex1.addNeighbour(vertex2.getInfo());
	}

	@Override
	public void addEdge(T info1, T info2, int... cost) {
		addDirectedEdge(info1, info2);
		addDirectedEdge(info2, info1);

	}

	@Override
	public void addVertex(T info) {
		if (vertexMapping.getVertexIndex(info) == IVertexMapping.VERTEX_NOT_FOUND) {
			adjacencyList.add(new Vertex<T>(info));
			vertexMapping.mapVertex(info);
		}
	}

	@Override
	public Integer[] getAdjecentVertices(int vertexIndex) {
		List<IVertex<T>> neighbourList = adjacencyList.get(vertexIndex).getNeighbours();
		if (neighbourList == null) {
			return new Integer[0];
		}
		Integer[] neighbours = new Integer[neighbourList.size()];
		int index = 0;
		for (IVertex<T> v : neighbourList) {
			neighbours[index++] = vertexMapping.getVertexIndex(v.getInfo());
		}
		return neighbours;
	}

	@Override
	public int getEdgeCost(int vertexIndex1, int vertexIndex2) {
		// TODO Auto-generated method stub
		return 1;
	}
}
