package com.intel.curssda.graph.traversal;

import com.intel.curssda.graph.IGraph;

public interface IGraphTraversal {
	public <T> void traverse(IGraph<T> graph);
}
