package com.intel.curssda.graph.traversal;

import com.intel.curssda.graph.IGraph;
import com.intel.curssda.graph.utils.IVertexMarkings.VertexType;

public class DFSTraversal extends AbstractTraversal {

	@Override
	public <T> void traverse(IGraph<T> graph) {
		handleStartTraversal();
		int startNode = 0;
		while ((startNode = graph.findUnvisitedVertex()) != IGraph.VERTEX_NOT_FOUND) {
			startConnexComponent();
			DFS(startNode, graph);
			endConnexComponent();
		}

	}

	private <T> void DFS(int startNode, IGraph<T> graph) {
		handleVertex(startNode);
		graph.markVertex(startNode, VertexType.PARTIALY_VISITED);
		for (Integer neighbour : graph.getAdjecentVertices(startNode)) {
			handleEdgeTraverse(startNode, neighbour);
			if (graph.getVertexMarking(neighbour) == VertexType.NOT_VISITED) {
				DFS(neighbour, graph);
			}
		}
		graph.markVertex(startNode, VertexType.VISITED);
	}
}
