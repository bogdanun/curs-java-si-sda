package com.intel.curssda.graph.traversal;

import com.intel.curssda.graph.IGraph;

public abstract class AbstractTraversal implements IGraphTraversal {

	@Override
	public abstract <T> void traverse(IGraph<T> graph);

	protected void startConnexComponent() {
		System.out.println("--------------- Start of Connex Component ------------------");

	}

	protected void endConnexComponent() {
		System.out.println("--------------- End of Connex Component ------------------");

	}

	protected void handleVertex(int startNode) {
		System.out.println(" currently at vertex: " + startNode);

	}

	protected void handleEdgeTraverse(int startNode, Integer neighbour) {
		System.out.println(" exploring edge: " + startNode + " -> " + neighbour);

	}
	
	protected void handleStartTraversal(){
		System.out.println("\n --- starting traversal: " + this.getClass().getName() +" --- \n");
	}

}
