package com.intel.curssda.graph.traversal;

import java.util.LinkedHashMap;
import java.util.Map;

import com.intel.curssda.graph.IGraph;
import com.intel.curssda.graph.utils.IVertexMarkings.VertexType;

public class Dijkstra {
	public class Selection {
		public static final int SELECTION_NOT_AVAILABLE = -1;
		int vertedIndex = SELECTION_NOT_AVAILABLE;
		int cost = SELECTION_NOT_AVAILABLE;
	}

	public <T> void findLowestCostRoute(IGraph<T> graph, int startVertex, int EndVertex) {
		// map will contain pairs of vertex/cost_so_far
		Map<Integer, Integer> selectedVertices = new LinkedHashMap<Integer, Integer>();
		selectedVertices.put(startVertex, 0);
		graph.markVertex(startVertex, VertexType.VISITED);

		while (graph.findUnvisitedVertex() != IGraph.VERTEX_NOT_FOUND) {
			Selection nextVertexSelection = getClosestVertexToSelection(graph, selectedVertices);
			//System.out.println("vertex selection: " + nextVertexSelection.vertedIndex);
			if (nextVertexSelection.vertedIndex != Selection.SELECTION_NOT_AVAILABLE) {
				selectedVertices.put(nextVertexSelection.vertedIndex, nextVertexSelection.cost);
				graph.markVertex(nextVertexSelection.vertedIndex, VertexType.VISITED);
				//System.out.println("selected vertices: " + selectedVertices);
			} else {
				// cannot select any vertex, must be in another connect component
				break;
			}
		}
		handleSolution(graph, selectedVertices, EndVertex);
	}

	private <T> void handleSolution(IGraph<T> graph, Map<Integer, Integer> selectedVertices, int endVertex) {
		if (!selectedVertices.containsKey(endVertex)) {
			System.out.println("couldn't find a route to vertex: " + endVertex);
		} else {
			System.out.print("lowest cost route: ");
			for (Integer v : selectedVertices.keySet()) {
				System.out.print("( " + graph.getVertexFromIndex(v) + ", " + selectedVertices.get(v) + ") ");
				if (endVertex == v) {
					break;
				}
			}
			System.out.println();
		}

	}

	protected <T> Selection getClosestVertexToSelection(IGraph<T> graph, Map<Integer, Integer> selectedVertices) {
		int distanceToClosestVertex = Integer.MAX_VALUE;
		Selection result = new Selection();
		for (Integer selectedVertex : selectedVertices.keySet()) {
			for (Integer neighbour : graph.getAdjecentVertices(selectedVertex)) {
				if (graph.getVertexMarking(neighbour) == VertexType.VISITED) {
					continue;
				}
				int currentCost = graph.getEdgeCost(selectedVertex, neighbour) + selectedVertices.get(selectedVertex);
				if (currentCost <= distanceToClosestVertex) {
					result.vertedIndex = neighbour;
					result.cost = currentCost;
					distanceToClosestVertex = currentCost;
				}
			}
		}
		return result;
	}

}
