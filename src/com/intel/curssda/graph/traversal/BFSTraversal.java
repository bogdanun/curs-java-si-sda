package com.intel.curssda.graph.traversal;

import java.util.LinkedList;

import com.intel.curssda.graph.IGraph;
import com.intel.curssda.graph.utils.IVertexMarkings.VertexType;

public class BFSTraversal extends AbstractTraversal {

	@Override
	public <T> void traverse(IGraph<T> graph) {
		handleStartTraversal();
		int startNode = 0;
		while ((startNode = graph.findUnvisitedVertex()) != IGraph.VERTEX_NOT_FOUND) {
			startConnexComponent();
			BFS(startNode, graph);
			endConnexComponent();
		}

	}

	private <T> void BFS(int startNode, IGraph<T> graph) {
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(startNode);
		graph.markVertex(startNode, VertexType.VISITED);
		while (queue.size() > 0) {
			int currentNode = queue.removeFirst();
			handleVertex(currentNode);
			for (int neighbour : graph.getAdjecentVertices(currentNode)) {
				if (graph.getVertexMarking(neighbour) != VertexType.VISITED) {
					handleEdgeTraverse(currentNode, neighbour);
					queue.add(neighbour);
					graph.markVertex(neighbour, VertexType.VISITED);
				}
			}
		}
	}
}
