package com.intel.curssda.graph.utils;

import java.util.HashMap;
import java.util.Map;

public class SimpleVertexMapping<T> implements IVertexMapping<T> {

	protected Map<T, Integer> vertexMapping = new HashMap<>();

	@Override
	public void mapVertex(T vertex) {
		if (vertexMapping.containsKey(vertex)) {
			return;
		}
		int nextIndex = vertexMapping.values().size();
		vertexMapping.put(vertex, nextIndex);
	}

	@Override
	public int getVertexIndex(T vertex) {
		if (!vertexMapping.containsKey(vertex)) {
			return VERTEX_NOT_FOUND;
		}
		return vertexMapping.get(vertex);
	}

	@Override
	public T getVertex(int index) {
		for (T vertex : vertexMapping.keySet()) {
			if (index == vertexMapping.get(vertex)) {
				return vertex;
			}
		}
		return null;
	}

}
