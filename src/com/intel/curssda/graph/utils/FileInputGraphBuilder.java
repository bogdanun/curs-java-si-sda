package com.intel.curssda.graph.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.intel.curssda.graph.IGraph;

public class FileInputGraphBuilder {
	public static final String GRAPH_DEFINITION_FILE = "src/com/intel/curssda/graph/utils/graphDefinition.txt";
	public static final String SPACE_CHAR = " ";

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public void extendGraph(IGraph graph) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(GRAPH_DEFINITION_FILE)));
		String fileLine;
		fileLine = in.readLine();
		int nrOfVertices = Integer.parseInt(fileLine);
		while ((fileLine = in.readLine()) != null) {
			String[] lineValues = fileLine.split(SPACE_CHAR);
			String currentVertex = lineValues[0];
			graph.addVertex(currentVertex);
			linkVertexToNeighbours(currentVertex, graph, lineValues);
		}
		in.close();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void linkVertexToNeighbours(String currentVertex, IGraph graph, String[] lineValues) {
		int currentEdgeCost = 0;
		for (int i = 1; i < lineValues.length; i++) {
			try {
				currentEdgeCost = Integer.parseInt(lineValues[i]);
				graph.addVertex(lineValues[++i]);
				graph.addDirectedEdge(currentVertex, lineValues[i], currentEdgeCost);
			} catch (NumberFormatException e) {
				graph.addVertex(lineValues[i]);
				graph.addDirectedEdge(currentVertex, lineValues[i]);
			}
		}
	}
}
