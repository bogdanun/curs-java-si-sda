package com.intel.curssda.graph.utils;

import com.google.inject.name.Named;

public class SimpleVertexMarkings implements IVertexMarkings {

	public static final int NOT_VISITED_MARKER = 0;
	public static final int PARTIALLY_VISITED_MARKER = 1;
	public static final int VISITED_MARKER = 2;
	public static int[] markings;

	public SimpleVertexMarkings(@Named("Number of vertices") int nrOfVertices) {
		markings = new int[nrOfVertices];
	}

	@Override
	public void markVertex(int vertexIndex, VertexType type) {
		if (type == VertexType.PARTIALY_VISITED) {
			markings[vertexIndex] = PARTIALLY_VISITED_MARKER;
		} else if (type == VertexType.VISITED) {
			markings[vertexIndex] = VISITED_MARKER;
		} else if (type == VertexType.NOT_VISITED) {
			markings[vertexIndex] = NOT_VISITED_MARKER;
		}
	}

	@Override
	public VertexType getVertexType(int vertexIndex) {
		if (markings[vertexIndex] == NOT_VISITED_MARKER) {
			return VertexType.NOT_VISITED;
		} else if (markings[vertexIndex] == PARTIALLY_VISITED_MARKER) {
			return VertexType.PARTIALY_VISITED;
		} else if (markings[vertexIndex] == VISITED_MARKER) {
			return VertexType.VISITED;
		}
		return null;
	}

	@Override
	public int getNrOfVertices() {
		return markings.length;
	}

}
