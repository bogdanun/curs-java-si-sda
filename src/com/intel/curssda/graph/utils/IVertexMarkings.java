package com.intel.curssda.graph.utils;

public interface IVertexMarkings {

	public enum VertexType {
		NOT_VISITED, PARTIALY_VISITED, VISITED
	};

	public void markVertex(int vertexIndex, VertexType type);

	public VertexType getVertexType(int vertexIndex);
	
	public int getNrOfVertices();

}
