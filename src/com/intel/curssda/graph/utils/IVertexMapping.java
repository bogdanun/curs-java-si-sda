package com.intel.curssda.graph.utils;

public interface IVertexMapping<T> {
	public static final int VERTEX_NOT_FOUND = -1;
	
	public void mapVertex(T vertex);

	public int getVertexIndex(T vertex);

	public T getVertex(int index);
}
