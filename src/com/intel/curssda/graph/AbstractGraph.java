package com.intel.curssda.graph;

import com.intel.curssda.graph.utils.IVertexMapping;
import com.intel.curssda.graph.utils.IVertexMarkings;
import com.intel.curssda.graph.utils.IVertexMarkings.VertexType;

public abstract class AbstractGraph<T> implements IGraph<T> {

	protected IVertexMapping<T> vertexMapping;
	protected IVertexMarkings vertexMarkings;
	protected int nrOfVertices;

	public T getVertexFromIndex(int index) {
		return vertexMapping.getVertex(index);
	}

	@Override
	public void markVertex(int vertexIndex, VertexType type) {
		vertexMarkings.markVertex(vertexIndex, type);
	}

	@Override
	public VertexType getVertexMarking(int vertexIndex) {
		return vertexMarkings.getVertexType(vertexIndex);
	}

	public int findUnvisitedVertex() {
		for (int i = 0; i < vertexMarkings.getNrOfVertices(); i++) {
			if (vertexMarkings.getVertexType(i) == VertexType.NOT_VISITED) {
				return i;
			}
		}
		return VERTEX_NOT_FOUND;
	}

}
