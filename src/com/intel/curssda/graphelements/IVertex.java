package com.intel.curssda.graphelements;

import java.util.List;

public interface IVertex<T> {
	public void setInfo(T info);

	public T getInfo();

	public void addNeighbour(T info);

	public List<IVertex<T>> getNeighbours();
}
