package com.intel.curssda.graphelements;

import java.util.ArrayList;
import java.util.List;

public class Vertex<T extends Comparable<T>> implements IVertex<T> {

	T info;
	List<IVertex<T>> neighbours = null;

	public Vertex(T info) {
		this.info = info;
	}

	private void initNeighbours() {
		neighbours = new ArrayList<>();
	}

	@Override
	public void setInfo(T info) {
		this.info = info;

	}

	@Override
	public T getInfo() {
		return this.info;
	}

	@Override
	public void addNeighbour(T info) {
		if (neighbours == null) {
			initNeighbours();
		}
		for (IVertex<T> v : neighbours) {
			if (info.compareTo(v.getInfo()) == 0) {
				return;
			}
		}
		neighbours.add(new Vertex<T>(info));
	}

	@Override
	public List<IVertex<T>> getNeighbours() {
		return neighbours;
	}

}
