package com.intel.cursjava.lesson2;

import com.intel.common.VectorUtils;
import com.intel.cursjava.Lesson;

public class Kadane implements Lesson {

	private int seqStart = 0;
	private int seqEnd = 0;

	public void init() {
		seqStart = 0;
		seqEnd = 0;
	}

	public int maxSubarray(int[] vect) {
		int maxEndingHere = 0;
		int maxSoFar = 0;
		for (int i = 0; i < vect.length; i++) {
			maxEndingHere = Math.max(0, maxEndingHere + vect[i]);
			if (maxEndingHere == 0)
				seqStart = i + 1;
			maxSoFar = Math.max(maxSoFar, maxEndingHere);
			if (maxEndingHere == maxSoFar)
				seqEnd = i;
		}
		return maxSoFar;
	}

	public void runKadaneOnArray(int[] v) {
		System.out.println("kadane will run on: ");
		VectorUtils.printVector(v);
		init();
		int maxSubarray = maxSubarray(v);
		System.out.printf("the max subarray is: %d, starting at %d, ending at %d\n", maxSubarray, seqStart, seqEnd);
	}

	public void runAssignment() {
		int[] v0 = new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		runKadaneOnArray(v0);
		int[] v1 = new int[] { 1, 2, 3, 4, 5, 6, 7, -2 };
		runKadaneOnArray(v1);
		int[] v2 = new int[] {};
		runKadaneOnArray(v2);
		int[] v3 = new int[] { 1 };
		runKadaneOnArray(v3);
	}

}
