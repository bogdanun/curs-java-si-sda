package com.intel.cursjava.lesson2;

import java.util.ArrayList;

public class MartianCouncil {
	private ArrayList<Martian> members = new ArrayList<>();
	private int nrOfMembers;

	public void setNrOfMembers(int n) {
		nrOfMembers = n;
	}

	public int getNrOfMembers() {
		return nrOfMembers;
	}

	public Martian ensureMember(int n) {
		Martian member = getMember(n);
		if (member == null) {
			member = new Martian(n);
			members.add(member);
		}
		return member;
	}

	public void addAncestorToMember(int memberNr, int ancestorNr) {
		Martian member = ensureMember(memberNr);
		Martian ancestor = ensureMember(ancestorNr);
		member.addAncestor(ancestor);
	}

	public void addDescendentToMember(int memberNr, int descendentNr) {
		Martian member = ensureMember(memberNr);
		Martian descendent = ensureMember(descendentNr);
		member.addDescendant(descendent);
	}

	public Martian getMember(int n) {
		for (Martian m : members) {
			if (m.getNrTag() == n) {
				return m;
			}
		}
		return null;
	}

	public ArrayList<Martian> getMembers() {
		return members;
	}
}
