package com.intel.cursjava.lesson2;

import java.util.ArrayList;

public class Martian {
	private ArrayList<Martian> ancestors = new ArrayList<>();
	private ArrayList<Martian> descendents = new ArrayList<>();
	private int nrTag;
	
	public Martian(int nr) {
		nrTag = nr;
	}

	public int getNrTag(){
		return nrTag;
	}

	public void addAncestor(Martian a) {
		ancestors.add(a);
	}

	public void addDescendant(Martian d) {
		descendents.add(d);
	}

	public ArrayList<Martian> getAncestors() {
		return ancestors;
	}

	public ArrayList<Martian> getDescendents() {
		return descendents;
	}
	
	public void print(){
		System.out.println("member: " + getNrTag());
		System.out.print("descendants: ");
		for (Martian m: descendents){
			System.out.print(m.getNrTag() + " ");
		}
		System.out.println();
		System.out.print("ancestors: ");
		for (Martian m: ancestors){
			System.out.print(m.getNrTag() + " ");
		}
		System.out.println();
	}
}
