package com.intel.cursjava.lesson2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.intel.cursjava.Lesson;

public class MartianConcilSpeakingOrder implements Lesson {

	public static final String GENEALOGY_TREE_FILE = "src/com/intel/cursjava/lesson2/MartianPlanetaryCouncilGenealogyTree.txt";
	public static final String SPACE_CHAR = " ";
	private MartianCouncil council;

	public MartianConcilSpeakingOrder() {
		council = new MartianCouncil();
	}

	private void readGenealogyTrees() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(GENEALOGY_TREE_FILE)));
		String fileLine;
		fileLine = in.readLine();
		int nrOfMembers = Integer.parseInt(fileLine);
		int currentMember = 1;
		council.setNrOfMembers(nrOfMembers);
		while ((fileLine = in.readLine()) != null) {
			String[] lineValues = fileLine.split(SPACE_CHAR);
			if (lineValues.length == 1) {
				council.ensureMember(currentMember);
			} else if (lineValues.length > 1) {
				council.ensureMember(currentMember);
				for (int i = 0; i < lineValues.length - 1; i++) {
					int descendent = Integer.parseInt(lineValues[i]);
					council.addDescendentToMember(currentMember, descendent);
					council.addAncestorToMember(descendent, currentMember);
				}
			}
			currentMember++;
		}
		in.close();
	}

	private boolean ancestorsSelected(Martian martian, ArrayList<Martian> selected) {
		for (Martian m : martian.getAncestors()) {
			if (!selected.contains(m)) {
				return false;
			}
		}
		return true;
	}

	private void printCouncilOrder() {
		System.out.println("council speaking order: ");
		ArrayList<Martian> selected = new ArrayList<>();
		int memberCycles = council.getNrOfMembers();
		while (memberCycles > 0 || selected.size() < council.getNrOfMembers()) {
			for (Martian m : council.getMembers()) {
				if (!selected.contains(m) && ancestorsSelected(m, selected)) {
					selected.add(m);
					System.out.print(m.getNrTag() + " ");
				}
			}
			memberCycles--;
		}
		System.out.println();
	}

	private void printCouncil() {
		for (Martian m : council.getMembers()) {
			m.print();
		}
	}

	@Override
	public void runAssignment() {
		try {
			readGenealogyTrees();
		} catch (IOException e) {
			System.out.print("error while reading genealogy trees from file");
			e.printStackTrace();
		}
		printCouncil();
		printCouncilOrder();
	}

}
