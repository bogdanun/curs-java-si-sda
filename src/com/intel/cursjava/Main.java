package com.intel.cursjava;

import java.util.LinkedList;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.intel.cursjava.lesson1.BubbleSort;
import com.intel.cursjava.lesson1.Factorial;
import com.intel.cursjava.lesson2.Kadane;
import com.intel.cursjava.lesson2.MartianConcilSpeakingOrder;
import com.intel.cursjava.lesson3.IStack;
import com.intel.cursjava.lesson3.LinkedListStack;
import com.intel.cursjava.lesson3.StackUser;
import com.intel.cursjava.lesson4.ConsumerProducerCycle;
import com.intel.curssda.sorting.SortingLesson;

class StackModule extends AbstractModule {
	@Override
	public void configure() {
		bind(new TypeLiteral<IStack<String>>() {
		}).toInstance(new LinkedListStack<String>());
	}
}

public class Main {
	public static LinkedList<Lesson> lessons = new LinkedList<>();

	public static void buildJavaCourseAssignments() {
		lessons.add(new Factorial());
		lessons.add(new BubbleSort());
		lessons.add(new Kadane());
		lessons.add(new MartianConcilSpeakingOrder());
		Injector injector = Guice.createInjector(new StackModule());
		StackUser stackUser = injector.getInstance(StackUser.class);
		lessons.add(stackUser);
		lessons.add(new ConsumerProducerCycle(2, 5));
	}

	public static void printSectionSeparation() {
		System.out.println("---------------------------------------------------------------");
	}

	public static void main(String[] argv) {
		// buildJavaCourseAssignments();
		buildSDACourseAssignments();
		printSectionSeparation();
		for (Lesson ls : lessons) {
			System.out.println("running: " + ls.getClass().getName());
			ls.runAssignment();
			printSectionSeparation();
		}
	}

	private static void buildSDACourseAssignments() {
		lessons.add(new SortingLesson());

	}
}
