package com.intel.cursjava.lesson1;

import com.intel.common.VectorUtils;
import com.intel.cursjava.Lesson;

public class BubbleSort implements Lesson {

	public int[] sort(int[] vector) {
		boolean modified;
		do {
			modified = false;
			for (int i = 0; i < vector.length - 1; i++) {
				if (vector[i] > vector[i + 1]) {
					int tmp = vector[i];
					vector[i] = vector[i + 1];
					vector[i + 1] = tmp;
					modified = true;
				}
			}
		} while (modified == true);
		return vector;
	}

	public void runVectorSorting(int[] v) {
		System.out.println("initial vector: ");
		System.out.print(VectorUtils.printVector(v));
		System.out.println("sorted vector: ");
		System.out.print(VectorUtils.printVector(sort(v)));
	}

	public void runAssignment() {
		int[] v0 = new int[] {};
		runVectorSorting(v0);
		int[] v1 = new int[] { 1 };
		runVectorSorting(v1);
		int[] v2 = new int[] { 2, 5, 8, 4, 3, 10, 12 };
		runVectorSorting(v2);
	}

}
