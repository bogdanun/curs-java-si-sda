package com.intel.cursjava.lesson1;

import com.intel.cursjava.Lesson;

public class Factorial implements Lesson {

	public Object computeFactorial(int n) {
		if (n < 0) {
			System.out.println("there is no factorial defined for numbers smaller than 0");
			return null;
		}
		long i = 1;
		long result = 1;
		while (i <= n) {
			result *= i;
			i++;
		}
		return result;
	}

	@Override
	public void runAssignment() {
		System.out.println("the factorial of 0 is: " + computeFactorial(0));
		System.out.println("the factorial of 3 is: " + computeFactorial(3));
		System.out.println("the factorial of 10 is: " + computeFactorial(10));
		System.out.println("the factorial of 20 is: " + computeFactorial(20));
		System.out.println("the factorial of -5 is: " + computeFactorial(-5));
	}

}
