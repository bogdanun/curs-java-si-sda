package com.intel.cursjava.lesson4;

public class Producer extends Thread {
	private String name;
	private Warehouse warehouse;
	private Object warehouseLock;
	private static final int produceCapacity = 3;

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
		this.warehouseLock = warehouse.getLock();
	}

	@Override
	public void run() {
		name = getName();
		try {
			while (true) {
				Thread.sleep(1000);
				synchronized (warehouseLock) {
					System.out.println(name + " produced " + produceCapacity);
					warehouse.increaseStock(produceCapacity);
				}
			}
		} catch (InterruptedException e) {
			System.out.println("Producer " + name + " was interrupted");
		}
	}
}
