package com.intel.cursjava.lesson4;

public class Consumer extends Thread {
	private String name;
	private Warehouse warehouse;
	private Object warehouseLock;
	private static final int consumeCapacity = 1;

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
		this.warehouseLock = warehouse.getLock();
	}

	@Override
	public void run() {
		name = getName();
		try {
			while (true) {
				Thread.sleep(500);
				synchronized (warehouseLock) {
					System.out.println(name + " retrieving " + consumeCapacity + " from warehouse");
					if (warehouse.getStock() >= consumeCapacity) {
						warehouse.decreaseStock(consumeCapacity);
						System.out.println(name + " consumed " + consumeCapacity);
					} else {
						System.out.println(name + " - insufficient goods in stock: " + warehouse.getStock());
					}
				}
			}
		} catch (InterruptedException e) {
			System.out.println("Consumer " + name + " was interrupted");
		}
	}
}
