package com.intel.cursjava.lesson4;

import java.util.ArrayList;

import com.intel.cursjava.Lesson;

public class ConsumerProducerCycle implements Lesson {

	ArrayList<Consumer> consumers;
	ArrayList<Producer> producers;
	Warehouse wh;

	public ConsumerProducerCycle(int nrOfProducers, int nrOfConsumers) {
		consumers = new ArrayList<Consumer>();
		producers = new ArrayList<Producer>();
		for (int i = 0; i < nrOfConsumers; i++) {
			Consumer c = new Consumer();
			c.setName("Consumer " + i);
			consumers.add(c);
		}
		for (int i = 0; i < nrOfProducers; i++) {
			Producer p = new Producer();
			p.setName("Producer " + i);
			producers.add(p);
		}
		wh = new Warehouse();
	}

	@Override
	public void runAssignment() {
		for (Producer p : producers) {
			p.setWarehouse(wh);
			p.start();
		}
		for (Consumer c : consumers) {
			c.setWarehouse(wh);
			c.start();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			interruptAllActors();
			try {
				joinAllActors();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void interruptAllActors() {
		for (Producer p : producers) {
			p.interrupt();
		}
		for (Consumer c : consumers) {
			c.interrupt();
		}
	}

	private void joinAllActors() throws InterruptedException {
		for (Producer p : producers) {
			p.join();
		}
		for (Consumer c : consumers) {
			c.join();
		}
	}

}
