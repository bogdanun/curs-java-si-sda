package com.intel.cursjava.lesson4;

public class Warehouse {
	private int stock;
	private static Object warehouseLock = new Object();

	public void increaseStock(int units) {
		stock += units;
		printCurrentStock();
	}

	public Object getLock(){
		return warehouseLock;
	}
	
	public synchronized void decreaseStock(int units) {
		stock -= units;
		printCurrentStock();
	}

	public int getStock() {
		return stock;
	}

	public void printCurrentStock() {
		System.out.println("Warehouse current stock: " + stock);
	}

}
