package com.intel.cursjava.lesson3;

import java.util.EmptyStackException;

public class LinkedListStack<T> implements IStack<T> {
	public class ListNode<V> {
		private V elem;
		private ListNode<V> next;

		public void setInfo(V elem) {
			this.elem = elem;
		}

		public void setNext(ListNode<V> ln) {
			this.next = ln;
		}

		public V getInfo() {
			return elem;
		}

		public ListNode<V> getNext() {
			return next;
		}
	}

	private ListNode<T> topElem = null;
	private int size = 0;

	@Override
	public void push(T elem) {
		ListNode<T> ln = new ListNode<T>();
		ln.setInfo(elem);
		ln.setNext(topElem);
		this.topElem = ln;
		size++;
	}

	@Override
	public T pop() {
		assert size >= 0;
		if (size == 0) {
			throw new EmptyStackException();
		}
		T elem = this.topElem.getInfo();
		this.topElem = this.topElem.getNext();
		size--;
		return elem;
	}

	@Override
	public T peek() {
		assert size >= 0;
		return topElem.getInfo();
	}

	@Override
	public boolean isEmpty() {
		assert size >= 0;
		return size == 0;
	}

}
