package com.intel.cursjava.lesson3;

import com.google.inject.Inject;
import com.intel.cursjava.Lesson;

public class StackUser implements Lesson {

	IStack<String> stringStack;

	@Inject
	public StackUser(IStack<String> stack) {
		this.stringStack = stack;
	}

	private void operateStringStack() {
		stringStack.push("1");
		stringStack.push("2");
		stringStack.push("trei");
		System.out.println("top stack element: " + stringStack.pop());
		System.out.println("top stack element: " + stringStack.pop());
		System.out.println("top stack element: " + stringStack.pop());
	}

	@Override
	public void runAssignment() {
		operateStringStack();
	}
}
