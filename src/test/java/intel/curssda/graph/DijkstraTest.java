package test.java.intel.curssda.graph;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.graph.AdjacencyMatrixGraph;
import com.intel.curssda.graph.IGraph;
import com.intel.curssda.graph.traversal.Dijkstra;
import com.intel.curssda.graph.utils.FileInputGraphBuilder;
import com.intel.curssda.graph.utils.SimpleVertexMapping;
import com.intel.curssda.graph.utils.SimpleVertexMarkings;

class DijkstraModule extends AbstractModule {
	static final int nrOfVertices = 8;

	@SuppressWarnings("rawtypes")
	@Provides
	protected AdjacencyMatrixGraph providerServiceForGraph() {
		SimpleVertexMapping<String> vMapping = new SimpleVertexMapping<>();
		SimpleVertexMarkings vMarkings = new SimpleVertexMarkings(nrOfVertices);
		AdjacencyMatrixGraph<String> adjMatGraph = new AdjacencyMatrixGraph<>(nrOfVertices, vMapping, vMarkings);
		return adjMatGraph;
	}

	@Provides
	protected Dijkstra providerServiceForDijkstra() {
		return new Dijkstra();
	}

	@Override
	protected void configure() {
		;
	}
}

public class DijkstraTest {
	protected IGraph<String> graph;
	protected Dijkstra dijkstra;

	@SuppressWarnings("unchecked")
	@Before
	public void initGraph() throws IOException {
		Injector injector = Guice.createInjector(new AdjacencyMatrixGraphModule());
		graph = injector.getInstance(AdjacencyMatrixGraph.class);
		dijkstra = injector.getInstance(Dijkstra.class);
		FileInputGraphBuilder builder = new FileInputGraphBuilder();
		builder.extendGraph(graph);
	}

	@Test
	public void testDijkstra() {
		dijkstra.findLowestCostRoute(graph, 0, 2);
	}
}
