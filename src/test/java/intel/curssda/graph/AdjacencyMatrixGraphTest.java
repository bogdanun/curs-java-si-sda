package test.java.intel.curssda.graph;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.graph.AdjacencyMatrixGraph;
import com.intel.curssda.graph.IGraph;
import com.intel.curssda.graph.traversal.BFSTraversal;
import com.intel.curssda.graph.traversal.DFSTraversal;
import com.intel.curssda.graph.traversal.IGraphTraversal;
import com.intel.curssda.graph.utils.FileInputGraphBuilder;
import com.intel.curssda.graph.utils.SimpleVertexMapping;
import com.intel.curssda.graph.utils.SimpleVertexMarkings;

class AdjacencyMatrixGraphModule extends AbstractModule {
	static final int nrOfVertices = 8;

	@SuppressWarnings("rawtypes")
	@Provides
	protected AdjacencyMatrixGraph providerServiceForGraph() {
		SimpleVertexMapping<String> vMapping = new SimpleVertexMapping<>();
		SimpleVertexMarkings vMarkings = new SimpleVertexMarkings(nrOfVertices);
		AdjacencyMatrixGraph<String> adjMatGraph = new AdjacencyMatrixGraph<>(nrOfVertices, vMapping, vMarkings);
		return adjMatGraph;
	}

	@Provides
	protected DFSTraversal providerServiceForTraversal() {
		return new DFSTraversal();
	}

	@Provides
	protected BFSTraversal providerServiceForBFSTraversal() {
		return new BFSTraversal();
	}

	@Override
	protected void configure() {
		;
	}
}

public class AdjacencyMatrixGraphTest {
	protected IGraph<String> graph;
	protected IGraphTraversal dfsTraversal, bfsTraversal;

	@SuppressWarnings("unchecked")
	@Before
	public void initGraph() throws IOException {
		Injector injector = Guice.createInjector(new AdjacencyMatrixGraphModule());
		graph = injector.getInstance(AdjacencyMatrixGraph.class);
		dfsTraversal = injector.getInstance(DFSTraversal.class);
		bfsTraversal = injector.getInstance(BFSTraversal.class);
		FileInputGraphBuilder builder = new FileInputGraphBuilder();
		builder.extendGraph(graph);
	}

	@Test
	public void testDFS() {
		dfsTraversal.traverse(graph);
	}

	@Test
	public void testBFS() {
		bfsTraversal.traverse(graph);
	}

}
