package test.java.intel.curssda.graph;

import java.io.IOException;

import org.junit.Before;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.graph.AdjacencyListsGraph;
import com.intel.curssda.graph.traversal.BFSTraversal;
import com.intel.curssda.graph.traversal.DFSTraversal;
import com.intel.curssda.graph.utils.FileInputGraphBuilder;
import com.intel.curssda.graph.utils.SimpleVertexMapping;
import com.intel.curssda.graph.utils.SimpleVertexMarkings;

class AdjacencyListsGraphModule extends AbstractModule {
	static final int nrOfVertices = 8;

	@SuppressWarnings("rawtypes")
	@Provides
	protected AdjacencyListsGraph providerServiceForGraph() {
		SimpleVertexMapping<String> vMapping = new SimpleVertexMapping<>();
		SimpleVertexMarkings vMarkings = new SimpleVertexMarkings(nrOfVertices);
		AdjacencyListsGraph<String> adjLstGraph = new AdjacencyListsGraph<>(vMapping, vMarkings);
		return adjLstGraph;
	}

	@Provides
	protected DFSTraversal providerServiceForDFSTraversal() {
		return new DFSTraversal();
	}

	@Provides
	protected BFSTraversal providerServiceForBFSTraversal() {
		return new BFSTraversal();
	}

	@Override
	protected void configure() {
		;
	}
}

public class AdjacencyListsGraphTest extends AdjacencyMatrixGraphTest {

	@SuppressWarnings("unchecked")
	@Before
	public void initGraph() throws IOException {
		Injector injector = Guice.createInjector(new AdjacencyListsGraphModule());
		graph = injector.getInstance(AdjacencyListsGraph.class);
		dfsTraversal = injector.getInstance(DFSTraversal.class);
		bfsTraversal = injector.getInstance(BFSTraversal.class);
		FileInputGraphBuilder builder = new FileInputGraphBuilder();
		builder.extendGraph(graph);
	}
}
