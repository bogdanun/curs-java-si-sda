package test.java.intel.curssda.hashset;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.intel.curssda.hash.IHashAlgoritm;
import com.intel.curssda.hash.IntegerModuloHashAlgorithm;
import com.intel.curssda.hashset.HashSet;
import com.intel.curssda.hashset.IHashSet;
import com.intel.curssda.listelements.ListElementCreator;
import com.intel.curssda.listelements.IListElementCreator;

class HashSetConfigModule extends AbstractModule {
	static final int hashSetSize = 100;
	static final int primeDivider = 97;

	@Override
	public void configure() {
		bind(Integer.class).annotatedWith(Names.named("HashSet Dimension")).toInstance(hashSetSize);
		bind(Integer.class).annotatedWith(Names.named("Hash Prime Divider")).toInstance(primeDivider);
		bind(IHashAlgoritm.class).to(IntegerModuloHashAlgorithm.class);
		bind(IListElementCreator.class).to(ListElementCreator.class);
	}
}

public class HashSetTest {
	IHashSet<Integer> hashSet;

	@SuppressWarnings("unchecked")
	@Before
	public void initSet() {
		Injector injector = Guice.createInjector(new HashSetConfigModule());
		// Unchecked
		hashSet = injector.getInstance(HashSet.class);
	}

	protected void printTestTitle(String title){
		System.out.print("-------------------- ");
		System.out.print(title);
		System.out.println(" test --------------------");
	}
	
	@Test
	public void testEmptyHashSetContains() {
		printTestTitle("empty set contains");
		assertTrue("", hashSet.contains(20) == false);
	}

	@Test
	public void testAddElement() {
		printTestTitle("add element");
		hashSet.add(20);
		assertTrue("", hashSet.contains(20) == true);
	}

	@Test
	public void testAddWithCollision() {
		printTestTitle("add with collision");
		hashSet.add(20);
		hashSet.add(HashSetConfigModule.primeDivider + 20);
		assertTrue("", hashSet.contains(20) == true);
		assertTrue("", hashSet.contains(HashSetConfigModule.primeDivider + 20) == true);
	}

	@Test
	public void testRemoveElementCollision() {
		printTestTitle("remove with collision");
		testAddWithCollision();
		hashSet.remove(20);
		assertTrue("", hashSet.contains(20) == false);
	}

	@Test
	public void testRemoveElement() {
		printTestTitle("remove element");
		testAddElement();
		hashSet.remove(20);
		assertTrue("", hashSet.contains(20) == false);
	}

	@After
	public void printHashSet() {
		((HashSet<Integer>) hashSet).printInternalStructure();
	}
}
