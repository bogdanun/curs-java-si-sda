package test.java.intel.curssda.hashset;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import com.intel.common.VectorUtils;
import com.intel.curssda.hash.IHashAlgoritm;
import com.intel.curssda.hash.IntegerModuloHashAlgorithm;
import com.intel.curssda.hashset.HashSet;
import com.intel.curssda.hashset.HashSetWithInsertionOrder;
import com.intel.curssda.listelements.IListElementCreator;
import com.intel.curssda.listelements.IListElementWithInsertionOrder;
import com.intel.curssda.listelements.ListElementWithInsertionOrderCreator;

class HashSetWithInsertionOrderConfigModule extends HashSetConfigModule {

	@Override
	public void configure() {
		bind(Integer.class).annotatedWith(Names.named("HashSet Dimension")).toInstance(hashSetSize);
		bind(Integer.class).annotatedWith(Names.named("Hash Prime Divider")).toInstance(primeDivider);
		bind(IHashAlgoritm.class).to(IntegerModuloHashAlgorithm.class);
		bind(IListElementCreator.class).to(ListElementWithInsertionOrderCreator.class);
	}
}

public class HashSetWithInsertionOrderTest extends HashSetTest {

	@SuppressWarnings("unchecked")
	@Before
	public void initSet() {
		Injector injector = Guice.createInjector(new HashSetWithInsertionOrderConfigModule());
		// Unchecked
		hashSet = injector.getInstance(HashSetWithInsertionOrder.class);
	}

	@Override
	@After
	public void printHashSet() {
		((HashSet<Integer>) hashSet).printInternalStructure();
		((HashSetWithInsertionOrder<Integer>) hashSet).printInsertionOrder();
	}

	@Test
	public void testOrderedElementsInsertion() {
		Integer[] testInsElems = VectorUtils.genSortedVectorOfInt(200);
		for (Integer elem : testInsElems) {
			hashSet.add(elem);
		}
		IListElementWithInsertionOrder<Integer> firstInserted = ((HashSetWithInsertionOrder<Integer>) hashSet)
				.getFirstInserted();
		while (firstInserted != null && firstInserted.getNextInserted() != null) {
			Integer smaller = firstInserted.getInfo();
			Integer bigger = firstInserted.getNextInserted().getInfo();
			assertTrue(smaller <= bigger);
			firstInserted = firstInserted.getNextInserted();
		}
	}
}
