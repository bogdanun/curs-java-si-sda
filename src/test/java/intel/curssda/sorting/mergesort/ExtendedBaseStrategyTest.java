package test.java.intel.curssda.sorting.mergesort;

import org.junit.Before;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.intel.curssda.sorting.ISorter;
import com.intel.curssda.sorting.ISubArraySorter;
import com.intel.curssda.sorting.InsertionSorter;
import com.intel.curssda.sorting.mergesort.MergeSorter;
import com.intel.curssda.sorting.mergesort.strategy.ExtendedBaseStrategy;
import com.intel.curssda.sorting.mergesort.strategy.IStrategy;

class ExtendedBaseStrategyModule extends AbstractModule {
	@Override
	public void configure() {
		bind(Integer.class).toInstance(5);
		bind(ISorter.class).to(InsertionSorter.class);
		bind(ISubArraySorter.class).to(InsertionSorter.class);
		bind(IStrategy.class).to(ExtendedBaseStrategy.class);
	}
}

public class ExtendedBaseStrategyTest extends BasicStrategyTest {
	@Before
	public void initSorter() {
		Injector injector = Guice.createInjector(new ExtendedBaseStrategyModule());
		mergeSorter = injector.getInstance(MergeSorter.class);
	}
}
