package test.java.intel.curssda.sorting.mergesort;

import org.junit.Before;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.intel.curssda.sorting.mergesort.MergeSorter;
import com.intel.curssda.sorting.mergesort.strategy.ReducedBaseStrategy;
import com.intel.curssda.sorting.mergesort.strategy.IStrategy;

class ReducedBaseStrategyModule extends AbstractModule {
	@Override
	public void configure() {
		bind(IStrategy.class).to(ReducedBaseStrategy.class);
	}
}

public class ReducedBaseStrategyTest extends BasicStrategyTest {
	@Before
	public void initSorter() {
		Injector injector = Guice.createInjector(new ReducedBaseStrategyModule());
		mergeSorter = injector.getInstance(MergeSorter.class);
	}
}
