package test.java.intel.curssda.sorting.mergesort;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.intel.common.VectorUtils;
import com.intel.curssda.sorting.ISorter;
import com.intel.curssda.sorting.mergesort.MergeSorter;
import com.intel.curssda.sorting.mergesort.strategy.BasicStrategy;
import com.intel.curssda.sorting.mergesort.strategy.IStrategy;

class BasicStrategyModule extends AbstractModule {
	@Override
	public void configure() {
		bind(IStrategy.class).to(BasicStrategy.class);
	}
}

public class BasicStrategyTest {
	ISorter mergeSorter;

	private Integer[] sortVectorOfInt(int size) {
		System.out.println("Sorting vector of size: " + size);
		Integer[] v = VectorUtils.genReverseVectorOfInt(size);
		mergeSorter.sort(v);
		System.out.println("Sorted Vector: ");
		VectorUtils.printVector(v);
		return v;
	}

	@Before
	public void initSorter() {
		Injector injector = Guice.createInjector(new BasicStrategyModule());
		mergeSorter = injector.getInstance(MergeSorter.class);
	}

	@Test
	public void testEmptyVectorSort() {
		sortVectorOfInt(0);
	}

	@Test
	public void testOneElementVectorSort() {
		sortVectorOfInt(1);
	}

	@Test
	public void testTwoElementVectorSort() {
		Integer[] v = sortVectorOfInt(2);
		assertTrue(VectorUtils.isVectorSorted(v));
	}

	@Test
	public void testThreeElementVectorSort() {
		Integer[] v = sortVectorOfInt(3);
		assertTrue(VectorUtils.isVectorSorted(v));
	}

	@Test
	public void testTenElementVectorSort() {
		Integer[] v = sortVectorOfInt(10);
		assertTrue(VectorUtils.isVectorSorted(v));
	}
	
	@Test
	public void testThousandElementVectorSort() {
		Integer[] v = sortVectorOfInt(1000);
		assertTrue(VectorUtils.isVectorSorted(v));
	}
}
