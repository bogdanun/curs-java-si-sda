package test.java.intel.curssda.sorting;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.intel.common.VectorUtils;
import com.intel.curssda.sorting.ISorter;
import com.intel.curssda.sorting.InsertionSorter;

public class InsertionSortingTest {
	ISorter insertionSorter;

	private Integer[] sortVectorOfInt(int size) {
		System.out.println("Sorting vector of size: " + size);
		Integer[] v = VectorUtils.genReverseVectorOfInt(size);
		insertionSorter.sort(v);
		System.out.println("Sorted Vector: ");
		VectorUtils.printVector(v);
		return v;
	}

	@Before
	public void initSorter() {
		insertionSorter = new InsertionSorter();
	}

	@Test
	public void testEmptyVectorSort() {
		sortVectorOfInt(0);
	}

	@Test
	public void testOneElementVectorSort() {
		sortVectorOfInt(1);
	}

	@Test
	public void testTwoElementVectorSort() {
		Integer[] v = sortVectorOfInt(2);
		assertTrue(VectorUtils.isVectorSorted(v));
	}

	@Test
	public void testTenElementVectorSort() {
		Integer[] v = sortVectorOfInt(10);
		assertTrue(VectorUtils.isVectorSorted(v));
	}
}
