package test.java.intel.curssda.sorting;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.common.VectorUtils;
import com.intel.curssda.sorting.HeapSorter;
import com.intel.curssda.sorting.ISorter;

class BasicStrategyModule extends AbstractModule {
	@Override
	public void configure() {
	}

	@Provides
	public HeapSorter providerServiceForHeapSorter() {
		return new HeapSorter();
	}
}

public class HeapSortingTest {
	ISorter heapSorter;

	private Integer[] sortVectorOfInt(int size) {
		System.out.println("Sorting vector of size: " + size);
		Integer[] v = VectorUtils.genRandomVectorOfInt(size, 100);
		if (size > 0) {
			v[0] = -1;
		}
		heapSorter.sort(v);
		System.out.println("Sorted Vector: ");
		VectorUtils.printVector(v);
		return v;
	}

	@Before
	public void initSorter() {
		Injector injector = Guice.createInjector(new BasicStrategyModule());
		heapSorter = injector.getInstance(HeapSorter.class);
	}

	@Test
	public void testEmptyVectorSort() {
		sortVectorOfInt(0);
	}

	@Test
	public void testOneElementVectorSort() {
		sortVectorOfInt(2);
	}

	@Test
	public void testTwoElementVectorSort() {
		Integer[] v = sortVectorOfInt(3);
		assertTrue(VectorUtils.isVectorSorted(v));
	}

	@Test
	public void testTenElementVectorSort() {
		Integer[] v = sortVectorOfInt(10);
		assertTrue(VectorUtils.isVectorSorted(v));
	}
}
