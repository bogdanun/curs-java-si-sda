package test.java.intel.curssda.tree;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.tree.BinaryTree;
import com.intel.curssda.tree.traversal.PreOrderBinaryTreeTraversal;
import com.intel.curssda.tree.traversal.TreeTraversal;

class BinaryTreePreOrderModule extends BinaryTreeModule {

	@SuppressWarnings("rawtypes")
	@Provides
	protected TreeTraversal providerServiceForBinaryTreeTraversal() {
		return new PreOrderBinaryTreeTraversal<String>();
	}
}

public class BinaryTreePreOrderTraversalTest extends BinaryTreeInOrderTraversalTest {
	@SuppressWarnings("unchecked")
	public void initTree() {
		Injector injector = Guice.createInjector(new BinaryTreePreOrderModule());
		tree = injector.getInstance(BinaryTree.class);
		traversal = injector.getInstance(TreeTraversal.class);
	}
	
	@Test
	@Override
	public void testTree2() {
		testTraversal(2, "5 1 7 ");
	}

	@Test
	@Override
	public void testTree3() {
		testTraversal(3, "18 0 100 10 10 11 ");
	}
}
