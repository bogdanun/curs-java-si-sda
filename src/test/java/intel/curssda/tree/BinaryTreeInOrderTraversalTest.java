package test.java.intel.curssda.tree;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.tree.BinaryTree;
import com.intel.curssda.tree.IBinaryTree;
import com.intel.curssda.tree.builder.BinaryTreeBuilderFromListString;
import com.intel.curssda.tree.traversal.InOrderBinaryTreeTraversal;
import com.intel.curssda.tree.traversal.TreeTraversal;

class BinaryTreeModule extends AbstractModule {

	public static final String[] TREES_FOR_TEST = { "(1)", "(1()(2))", "(5(1)(7))", "(18(0(100)())(10(10)(11)))" };
	public static int testTreeIndex = 0;

	@SuppressWarnings("rawtypes")
	@Provides
	protected BinaryTree providerServiceForBinaryTree() {
		BinaryTree<String> tree = new BinaryTree<String>();
		String testTreeRepr = TREES_FOR_TEST[testTreeIndex];
		BinaryTreeBuilderFromListString<String> treeBuilder = new BinaryTreeBuilderFromListString<String>(testTreeRepr);
		tree.setRoot(treeBuilder.buildAndGetRoot());
		return tree;
	}

	@Override
	protected void configure() {
		;
	}
}

class BinaryTreeInOrderModule extends BinaryTreeModule {

	@SuppressWarnings("rawtypes")
	@Provides
	protected TreeTraversal providerServiceForBinaryTreeTraversal() {
		return new InOrderBinaryTreeTraversal<String>();
	}

}

public class BinaryTreeInOrderTraversalTest {
	protected IBinaryTree<String> tree;
	protected TreeTraversal<String> traversal;

	@SuppressWarnings("unchecked")
	public void initTree() {
		Injector injector = Guice.createInjector(new BinaryTreeInOrderModule());
		tree = injector.getInstance(BinaryTree.class);
		traversal = injector.getInstance(TreeTraversal.class);
	}

	protected void testTraversal(int treeIndex, String refTaversalOrder) {
		BinaryTreeInOrderModule.testTreeIndex = treeIndex;
		System.out.println(" --- traversing tree: " + BinaryTreeInOrderModule.TREES_FOR_TEST[treeIndex]);
		initTree();
		traversal.traverse(tree.getRoot());
		List<String> traversalOrder = traversal.getTraversalOrder();
		assertTrue("Traversal order must be the same as the reference Traversal order",
				refTaversalOrder.equals(listToString(traversalOrder)));
		System.out.println("\n --- traversing complete.");
	}

	protected String listToString(List<String> lst) {
		StringBuilder sBld = new StringBuilder();
		for (String el : lst) {
			sBld.append(el + " ");
		}
		return sBld.toString();
	}

	@Test
	public void testTree0() {
		testTraversal(0, "1 ");
	}

	@Test
	public void testTree1() {
		testTraversal(1, "1 2 ");
	}

	@Test
	public void testTree2() {
		testTraversal(2, "1 5 7 ");
	}

	@Test
	public void testTree3() {
		testTraversal(3, "100 0 18 10 10 11 ");
	}

}
