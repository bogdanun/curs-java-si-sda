package test.java.intel.curssda.tree;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.intel.curssda.tree.BinaryTree;
import com.intel.curssda.tree.traversal.PostOrderBinaryTreeTraversal;
import com.intel.curssda.tree.traversal.TreeTraversal;

class BinaryTreePostOrderModule extends BinaryTreeModule {

	@SuppressWarnings("rawtypes")
	@Provides
	protected TreeTraversal providerServiceForBinaryTreeTraversal() {
		return new PostOrderBinaryTreeTraversal<String>();
	}
}

public class BinaryTreePostOrderTraversalTest extends BinaryTreeInOrderTraversalTest {
	@SuppressWarnings("unchecked")
	public void initTree() {
		Injector injector = Guice.createInjector(new BinaryTreePostOrderModule());
		tree = injector.getInstance(BinaryTree.class);
		traversal = injector.getInstance(TreeTraversal.class);
	}
	
	@Test
	@Override
	public void testTree1() {
		testTraversal(1, "2 1 ");
	}

	@Test
	@Override
	public void testTree2() {
		testTraversal(2, "1 7 5 ");
	}

	@Test
	@Override
	public void testTree3() {
		testTraversal(3, "100 0 10 11 10 18 ");
	}


}
