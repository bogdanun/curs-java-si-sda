package test.java;

import org.junit.*;

import com.intel.cursjava.lesson3.IStack;
import com.intel.cursjava.lesson3.LinkedListStack;

import static org.junit.Assert.assertTrue;

import java.util.EmptyStackException;

public class StackTest {
	IStack<String> stringStack;

	@Before
	public void initEmptyStack() {
		stringStack = new LinkedListStack<>();
	}

	@Test(expected = EmptyStackException.class)
	public void testEmptyListPop() {
		stringStack.pop();
	}

	@Test
	public void testPushPop() {
		stringStack.push("1");
		stringStack.push("2");
		assertTrue("first poped elem must be 2", stringStack.pop().equals("2"));
		assertTrue("second poped elem must be 1", stringStack.pop().equals("1"));
	}

}
